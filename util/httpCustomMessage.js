module.exports = {
    M200: "Success",
    M400: "Invalid Request",
    M401: "Unauthorized",
    M403: "Forbidden",
    M404: "Not Found",
    M409: "Record Conflict",
    M500: "Internal Server Error & Request Not Fulfilled",
};
