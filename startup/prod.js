const helmel = require("helmet");
const compression = require("compression");
const cors = require("cors");

module.exports = function (app) {
  app.get("env") === "production"
    ? app.use(
        cors({
          origin: "https://warm-crag-20467.herokuapp.com",
          optionsSuccessStatus: 200,
        })
      )
    : app.use(cors());
  app.use(helmel());
  app.use(compression());
};
