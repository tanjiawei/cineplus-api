const Joi = require("joi");
const mongoose = require("mongoose");

const Customer = mongoose.model(
    "Customer",
    new mongoose.Schema({
        firstName: {
            type: String,
            required: true,
            minlength: 3,
            maxlength: 50,
        },
        lastName: {
            type: String,
            required: true,
            minlength: 3,
            maxlength: 50,
        },
        isGold: {
            type: Boolean,
            default: false,
        },
        phone: {
            type: String,
            required: true,
            minlength: 5,
            maxlength: 50,
        },
        email: {
            type: String,
            required: true,
            minlength: 5,
            maxlength: 255,
            unique: true,
        },
        isActive: {
            type: Boolean,
            default: true,
        },
        joinAt: {
            type: Date,
            default: Date.now,
        },
    })
);

function validateCustomer(customer) {
    const schema = {
        firstName: Joi.string().min(3).max(50).required(),
        lastName: Joi.string().min(3).max(50).required(),
        phone: Joi.string().min(5).max(50).required(),
        email: Joi.string().min(5).max(255).required(),
        isGold: Joi.boolean(),
        isActive: Joi.boolean(),
    };

    return Joi.validate(customer, schema);
}

exports.Customer = Customer;
exports.validate = validateCustomer;
