const { Rental, validate, validateGet } = require("../models/rental");
const auth = require("../middleware/auth");
const { Movie } = require("../models/movie");
const { Customer } = require("../models/customer");
const mongoose = require("mongoose");
const Fawn = require("fawn");
const express = require("express");
const httpMsg = require("../util/httpCustomMessage");
const router = express.Router();

Fawn.init(mongoose);

router.use(auth);

// this is a get method as get won't accept req.body in axios.
router.put("/", auth, async (req, res) => {
    const { error } = validateGet(req.body);
    if (error) return res.status(400).send(httpMsg.M400);

    if (Object.values(req.body).join("").length <= 1) return res.send([]);

    const { status, movieName, customerName, customerPhone } = req.body;

    const template = {
        "customer.name": new RegExp(customerName, "gi"),
        "customer.phone": new RegExp(customerPhone, "gi"),
        "movie.title": new RegExp(movieName, "gi"),
        rentalFee: -1,
    };

    if (status === 1) {
        delete template.rentalFee;
    }
    if (status === 0) {
        template.rentalFee = { $gte: 0 };
    }
    const rentals = await Rental.find(template).sort("-dateOut");

    res.send(rentals);
});

router.post("/", async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(httpMsg.M400);

    const customer = await Customer.findById(req.body.customerId);
    if (!customer) return res.status(400).send(httpMsg.M400);

    const movie = await Movie.findById(req.body.movieId);
    if (!movie) return res.status(400).send(httpMsg.M400);

    if (movie.numberInStock === 0) return res.status(400).send(httpMsg.M400);

    let rental = new Rental({
        customer: {
            _id: customer._id,
            name: customer.name,
            phone: customer.phone,
        },
        movie: {
            _id: movie._id,
            title: movie.title,
            dailyRentalRate: movie.dailyRentalRate,
        },
    });

    try {
        new Fawn.Task()
            .save("rentals", rental)
            .update(
                "movies",
                { _id: movie._id },
                {
                    $inc: { numberInStock: -1 },
                }
            )
            .run();

        res.send(rental);
    } catch (ex) {
        res.status(500).send(httpMsg.M500);
    }
});

router.get("/:id", async (req, res) => {
    const rental = await Rental.findById(req.params.id);

    if (!rental) return res.status(404).send(httpMsg.M404);

    res.send(rental);
});

module.exports = router;
