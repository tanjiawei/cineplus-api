const Joi = require("joi");
const express = require("express");
const validate = require("../middleware/validate");
const { Rental } = require("../models/rental");
const { Movie } = require("../models/movie");
const auth = require("../middleware/auth");
const Fawn = require("fawn");
const httpMsg = require("../util/httpCustomMessage");
const router = express.Router();

Fawn.init(mongoose);

router.post("/", [auth, validate(validateReturn)], async (req, res) => {
    const { rentalId, customerId, movieId } = req.body;
    const rental = await Rental.lookUp(rentalId, customerId, movieId);

    if (!rental) return res.status(404).send(httpMsg.M404);

    if (rental.dateReturned) return res.status(400).send(httpMsg.M400);

    rental.return();

    try {
        new Fawn.Task()
            .save("rentals", rental)
            .update(
                "movies",
                { _id: movieId },
                {
                    $inc: { numberInStock: +1 },
                }
            )
            .run();

        res.send(rental);
    } catch (ex) {
        res.status(500).send(httpMsg.M500);
    }
});

function validateReturn(req) {
    const schema = {
        rentalId: Joi.objectId().required(),
        customerId: Joi.objectId().required(),
        movieId: Joi.objectId().required(),
    };

    return Joi.validate(req, schema);
}

module.exports = router;
