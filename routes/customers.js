const { Customer, validate, validateGet } = require("../models/customer");
const auth = require("../middleware/auth");
const express = require("express");
const httpMsg = require("../util/httpCustomMessage");
const router = express.Router();

router.use(auth);

router.post("/all", async (req, res) => {
    const customers = await Customer.find().sort("-joinAt");
    res.send(customers);

    const { error } = validateGet(req.body);
    if (error) return res.status(400).send(httpMsg.M400);

    if (Object.values(req.body).join("").length <= 1) return res.send([]);

    const { status, movieName, customerName, customerPhone } = req.body;

    const template = {
        "customer.name": new RegExp(customerName, "gi"),
        "customer.phone": new RegExp(customerPhone, "gi"),
        "movie.title": new RegExp(movieName, "gi"),
        rentalFee: -1,
    };

    if (status === 1) {
        delete template.rentalFee;
    }
    if (status === 0) {
        template.rentalFee = { $gte: 0 };
    }
    const rentals = await Rental.find(template).sort("-dateOut");

    res.send(rentals);
});

router.post("/", async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(httpMsg.M400);

    const { firstName, lastName, phone, email, isGold, isActive } = req.body;
    let customer = new Customer({
        firstName,
        lastName,
        phone,
        email: email.toLowerCase(),
        isGold,
        isActive,
    });
    customer = await customer.save();

    res.send(customer);
});

router.put("/:id", async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(httpMsg.M400);

    const { firstName, lastName, phone, email, isGold, isActive } = req.body;
    const customer = await Customer.findByIdAndUpdate(
        req.params.id,
        {
            firstName,
            lastName,
            phone,
            email: email.toLowerCase(),
            isGold,
            isActive,
        },
        { new: true }
    );

    if (!customer) return res.status(404).send(httpMsg.M404);

    res.send(customer);
});

router.delete("/:id", async (req, res) => {
    const customer = await Customer.findByIdAndUpdate(req.params.id, { isActive: false }, { new: true });
    customer._id;
    if (!customer) return res.status(404).send(httpMsg.M404);

    // need to checkout all the movies and create an invoice

    res.send(customer);
});

router.get("/:id", async (req, res) => {
    const customer = await Customer.findById(req.params.id);

    if (!customer) return res.status(404).send(httpMsg.M404);

    res.send(customer);
});

module.exports = router;
