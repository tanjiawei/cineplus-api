const request = require("supertest");
const mongoose = require("mongoose");
const { User } = require("../../models/user");
const { Genre } = require("../../models/genre");

describe("auth middleware", () => {
  let token;
  let payload;

  beforeEach(async () => {
    server = require("../../index");
    payload = {
      _id: mongoose.Types.ObjectId().toHexString(),
      name: "genre1",
    };
    await new Genre(payload).save();
  });
  afterEach(async () => {
    await Genre.remove({});
    await server.close();
  });

  const exec = () => {
    return request(server)
      .delete("/api/genres/" + payload._id)
      .set("x-auth-token", token);
  };

  beforeEach(() => {
    const user = {
      _id: mongoose.Types.ObjectId().toHexString(),
      isAdmin: true,
    };
    token = new User(user).generateAuthToken();
  });

  it("should return 403 if user is not an admin", async () => {
    token = new User().generateAuthToken();
    const res = await exec();
    expect(res.status).toBe(403);
  });

  it("should return 200 if user is an admin", async () => {
    const res = await exec();
    expect(res.status).toBe(200);
  });
});
