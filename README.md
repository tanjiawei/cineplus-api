# Cineplus API

## Getting Started

This backend application provides the RESTful API to consume for the the famous online rental ecommerce company Cineplus Inc headquartered in Toronto.

## Frontend project

[Cineplus](https://gitlab.com/tanjiawei/cineplus)

## Routes examples

Below routes is open only to non-user for view. You need permission to post/put/delete data in the database, which will not be open to view. For better application, please download the package and set up mongo databaes.

BaseURL:

- https://protected-oasis-48499.herokuapp.com/api/

apiLink:

- `/genres` - post/put/delete by `admin`, post/put by `users` and get by all
- `/movies` - post/delete/put by `admin and users` and get by all
- `/customers` - post/delete/put/get by all (non-essential at this moment)
- `/rentals` - post/get by/for `customers`
- `/returns` - post by/for `customers`
- `/users` - post only
- `/auth` - post only

### Prerequisites

You need Node.js to install the various packages used in this application. You can go to [Nodejs.org](https://nodejs.org/en/download/) to download and install node. The `npm` function will be used to install/initialize this application. This server uses Mongo DB for storing data and testing.

### Installing

Run below command in your terminal to install various dependencies

```
npm install
```

### Running

Run below command in your terminal to start this program

```
npm start
```

If you have nodemon installed in your machine, you can also run for development

```
nodemon index.js
```

or

```
npm run d
```

### Running the tests

The automated tests are to make sure that any update on the source codes will be tested thoroughly to prevent bugs. But Mongo DB (local) has to be connected before tests can be run. Run below command.

```
npm test
```

## Deployment

You can use deploy this project with Heroku. For more information on deployment on Heroku, please visit their [page](http://heroku.com/). You need to have an account and also install Heroku Cli by [link](https://devcenter.heroku.com/articles/heroku-cli)

Pre-production set up:

Go to file package.json under `engines`, update the versions of both node and npm by typing `node --version` and `npm --version` in the terminal.

Run below command to deploy for production,

First, log in by

```
heroku login
```

Second, initialize git by

```
git init
```

Third, build production and push to Heroku, make sure that your git branch is `master`.

```
git add .
git commit -m "First commit."
git create
git push heroku master

```

Fourth, setting environment variables

```
heroku config:set cineplus_jwtPrivateKey=<###a long string with numbers, letters and symbols###>
heroku config:set NODE_ENV=production
heroku config:set cineplus_db=<###mongodb atlas connectino string###>

```

## Built With

- [Lodash](https://lodash.com/) - Movies array manipulation
- [express](https://www.npmjs.com/package/express) - Provides RESRful API services
- [helmet](https://www.npmjs.com/package/helmet) - setting various HTTP headers
- [mongoose](https://www.npmjs.com/package/mongoose) - Connects to Mongo DB server
- [joi](https://www.npmjs.com/package/joi) - Backend form validation
- [joi-objectid](https://www.npmjs.com/package/joi-objectid) - A MongoDB ObjectId validator for Joi
- [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken) - return jwt upon login
- [lodash](https://www.npmjs.com/package/lodash) - handle strings and objects
- [esm](https://www.npmjs.com/package/esm) - ECMAScript module loader
- [nodemon](https://www.npmjs.com/package/nodemon) - Development dependencies - automatically restarting the node application
- [bcrypt](https://www.npmjs.com/package/bcrypt) - hash passwords
- [config](https://www.npmjs.com/package/config) - organizes hierarchical configurations
- [express-async-errors](https://www.npmjs.com/package/express-async-errors) - handle express async error
- [fawn](https://www.npmjs.com/package/fawn) - Promise based Library for transactions in MongoDB
- [moment](https://www.npmjs.com/package/moment) - parsing, validating, manipulating, and formatting dates
- [winston](https://www.npmjs.com/package/winston) - log information and generate log files in the server
- [winston-mongodb](https://www.npmjs.com/package/winston-mongodb) - log information and generate log files in the mongoDB
- [compression](https://www.npmjs.com/package/compression) - compress HTTP response

## Authors

- **Westley Tan** - _Initial work_

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
